package me.bradleyh.igsg;

import me.bradleyh.igsg.game.GameManager;
import me.bradleyh.igsg.game.GameStage;
import me.bradleyh.igsg.game.GameState;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

public class Listeners implements Listener
{
    @EventHandler
    public void onJoin(PlayerJoinEvent e)
    {
        e.getPlayer().getInventory().clear();
        e.getPlayer().setHealth(20);
        e.getPlayer().setFoodLevel(20);
        e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', "&a&lJOIN &r&8// &7" + e.getPlayer().getName()));
        if (SGPlugin.getInstance().gm.getState() != GameState.LOBBY && SGPlugin.getInstance().gm.getState() != GameState.LOBBY_COUNTDOWN)
        {
            e.getPlayer().setGameMode(GameMode.SPECTATOR);
            e.getPlayer().teleport(SGPlugin.getInstance().gm.getCurrentArena().getCenter());
            SGPlugin.getInstance().gm.setPlayerState(e.getPlayer(), GameManager.PlayerType.JOIN_SPECTATOR);
            return;
        }
        SGPlugin.getInstance().gm.setPlayerState(e.getPlayer(), GameManager.PlayerType.PLAYER);
        e.getPlayer().setGameMode(GameMode.SURVIVAL);
        e.getPlayer().teleport(SGPlugin.spawn);
        e.getPlayer().removePotionEffect(PotionEffectType.SPEED);
        if (SGPlugin.getInstance().gm.getState() == GameState.LOBBY && Bukkit.getOnlinePlayers().size() > 3)
        {
            SGPlugin.getInstance().gm.startLobbyCountdown();
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e)
    {
        e.setQuitMessage(ChatColor.translateAlternateColorCodes('&', "&a&lQUIT &r&8// &7" + e.getPlayer().getName()));
        e.getPlayer().getInventory().clear();
        SGPlugin.getInstance().gm.removePlayerState(e.getPlayer());
        if (SGPlugin.getInstance().gm.getState() == GameState.LOBBY_COUNTDOWN && Bukkit.getOnlinePlayers().size() < 4)
        {
            SGPlugin.getInstance().gm.lobbyCountdown.cancel();
            Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&a&lGAME &r&8// &7The game has stopped due to lack of players."));
        }
        if (SGPlugin.getInstance().gm.getState() == GameState.INGAME && SGPlugin.getInstance().gm.getPlayerState(e.getPlayer()) == GameManager.PlayerType.PLAYER)
        {
            GameManager.players--;
            if (GameManager.players == 1)
            {
                SGPlugin.getInstance().gm.stopGame();
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInteract(PlayerInteractEvent e)
    {
        if (e.getItem() != null && e.getItem().getType() == Material.MUSHROOM_SOUP && SGPlugin.getInstance().gm.getState() == GameState.INGAME)
        {
            ItemStack item = e.getPlayer().getInventory().getItemInMainHand();
            item.setAmount(item.getAmount() - 1);

            if (item.getAmount() == 0)
                e.getPlayer().getInventory().setItemInMainHand(new ItemStack(Material.AIR));
            else
                e.getPlayer().getInventory().setItemInMainHand(item);

            e.getPlayer().setHealth(e.getPlayer().getHealth() + 6);
            return;
        }

        if (e.getAction() == Action.RIGHT_CLICK_BLOCK && SGPlugin.getInstance().gm.getState() == GameState.INGAME)
        {
            // Check if Spectator
            if (SGPlugin.getInstance().gm.getPlayerState(e.getPlayer()) != GameManager.PlayerType.PLAYER)
            {
                return;
            }

            if (e.getClickedBlock().getType() == Material.ENDER_CHEST || e.getClickedBlock().getType() == Material.CHEST)
            {
                e.setCancelled(true);
                if (SGPlugin.usedChests.contains(e.getClickedBlock()))
                {
                    e.getPlayer().openInventory(Bukkit.createInventory(null, 27));
                } else
                {
                    e.getPlayer().openInventory(SGPlugin.getInstance().getRegenEngine().getChestInventory());
                    SGPlugin.usedChests.add(e.getClickedBlock());
                }
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e)
    {
        if (SGPlugin.getInstance().gm.getState() == GameState.INGAME)
        {
            switch (e.getBlock().getType())
            {
                case LEAVES:
                case LEAVES_2:
                case VINE:
                case SEEDS:
                case WHEAT:
                case LONG_GRASS:
                case MELON_BLOCK:
                case CARROT:
                case POTATO:
                    SGPlugin.getInstance().gm.addBlockToRestore(e.getBlock(), true);
                    return;
                case BOAT:
                    return;
                default:
                    if (!e.getPlayer().hasPermission("sg.breakblocks"))
                    {
                        e.setCancelled(true);
                    } else
                    {
                        SGPlugin.getInstance().gm.addBlockToRestore(e.getBlock(), true);
                    }
                    return;
            }
        } else
        {
            if (!e.getPlayer().hasPermission("sg.breakblocks"))
            {
                e.setCancelled(true);
            } else
            {
                SGPlugin.getInstance().gm.addBlockToRestore(e.getBlock(), true);
            }
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e)
    {
        ItemStack itemStack = e.getItemInHand();
        Player player = e.getPlayer();

        if (itemStack.getType() == Material.TNT)
        {
            if (itemStack.getAmount() == 1)
            {
                player.getInventory().setItemInMainHand(null);
            } else
            {
                itemStack.setAmount(itemStack.getAmount() - 1);
                player.getInventory().setItemInMainHand(itemStack);
            }

            e.setCancelled(true);
            e.getBlock().getWorld().spawnEntity(e.getBlock().getLocation(), EntityType.PRIMED_TNT);
            return;
        }

        if (SGPlugin.getInstance().gm.getState() == GameState.INGAME)
        {
            switch (e.getBlock().getType())
            {
                case WEB:
                case CAKE:
                case CAKE_BLOCK:
                case FLINT_AND_STEEL:
                case FIRE:
                    SGPlugin.getInstance().gm.addBlockToRestore(e.getBlock(), false);
                    return;
                default:
                    if (!e.getPlayer().hasPermission("sg.placeblocks"))
                    {
                        e.setCancelled(true);
                    } else
                    {
                        SGPlugin.getInstance().gm.addBlockToRestore(e.getBlock(), false);
                    }
                    break;
            }
        } else
        {
            if (!e.getPlayer().hasPermission("sg.placeblocks"))
            {
                e.setCancelled(true);
            } else
            {
                SGPlugin.getInstance().gm.addBlockToRestore(e.getBlock(), false);
            }
        }
    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent e)
    {
        if (e.getRightClicked().getType() != EntityType.ITEM_FRAME)
        {
            return;
        }

        e.setCancelled(true);
    }

    @EventHandler
    public void onHangingBreak(HangingBreakByEntityEvent e)
    {
        e.setCancelled(true);
    }

    @EventHandler
    public void onHangingPlace(HangingPlaceEvent e)
    {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDeath(EntityDeathEvent e)
    {
        if (e.getEntityType() == EntityType.SQUID)
        {
            e.getDrops().clear();
        }
    }

    @EventHandler
    public void onBurn(BlockBurnEvent e)
    {
        e.setCancelled(true);
    }

    @EventHandler
    public void onFireSpread(BlockSpreadEvent e)
    {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPVP(EntityDamageByEntityEvent e)
    {
        if (!(SGPlugin.getInstance().gm.getState() == GameState.INGAME)) e.setCancelled(true);
        if (e.getEntity().getType() == EntityType.PLAYER && SGPlugin.getInstance().gm.getStage() == GameStage.PREDM)
            e.setCancelled(true);
        if (((Player) e.getDamager()).hasPotionEffect(PotionEffectType.SPEED) && SGPlugin.getInstance().gm.getState() == GameState.INGAME)
            ((Player) e.getDamager()).removePotionEffect(PotionEffectType.SPEED);
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e)
    {
        if ((SGPlugin.getInstance().gm.getState() != GameState.LOBBY_COUNTDOWN || SGPlugin.getInstance().gm.getState() != GameState.LOBBY_COUNTDOWN) && SGPlugin.getInstance().gm.getPlayerState(e.getEntity()) == GameManager.PlayerType.PLAYER)
        {
            for (ItemStack i : e.getEntity().getInventory().getContents())
            {
                if (i != null)
                {
                    e.getEntity().getWorld().dropItemNaturally(e.getEntity().getLocation(), i);
                }
            }
            e.getEntity().setHealth(20);
            e.setDeathMessage(ChatColor.translateAlternateColorCodes('&', "&a&lDEATH &r&8// &a" + e.getEntity().getName() + "&7" + (e.getEntity().getKiller() == null ? " has died." : " was killed by &a" + e.getEntity().getKiller().getName() + "&7.")));
            e.setKeepInventory(false);
            SGPlugin.getInstance().gm.setPlayerState(e.getEntity(), GameManager.PlayerType.SPECTATOR);
            ArrayList<Player> position = SGPlugin.getInstance().gm.getPosition();
            position.add(0, e.getEntity());
            SGPlugin.getInstance().gm.setPosition(position);
            GameManager.players--;
            if (GameManager.players == 1)
            {
                for (Player p : Bukkit.getOnlinePlayers())
                {
                    if (SGPlugin.getInstance().gm.getPlayerState(p) == GameManager.PlayerType.PLAYER)
                    {
                        position.add(0, p);
                    }
                }
                SGPlugin.getInstance().gm.stopGame();
            }
            if (SGPlugin.getInstance().gm.getPlayerState(e.getEntity()) != GameManager.PlayerType.PLAYER)
                e.getEntity().setHealth(20);
            Location l = SGPlugin.getInstance().gm.getCurrentArena().getCenter();
            l.setWorld(Bukkit.getWorld(SGPlugin.getInstance().gm.getCurrentArena().getWorldName()));
            e.getEntity().teleport(l);
        }
    }

    @EventHandler
    public void mobSpawn(EntitySpawnEvent e)
    {
        if (e.getEntityType() != EntityType.PLAYER)
        {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e)
    {
        if (SGPlugin.getInstance().gm.getState() == GameState.GAME_STARTING || SGPlugin.getInstance().gm.getStage() == GameStage.PREDM)
        {
            if (!(e.getFrom().getX() == e.getTo().getX()) && !(e.getFrom().getZ() == e.getTo().getZ()))
            {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onWeather(WeatherChangeEvent e)
    {
        e.setCancelled(true);
        e.getWorld().setWeatherDuration(0);
    }

    @EventHandler
    public void onPlayerLoseHealth(EntityDamageEvent e)
    {
        if (e.getEntityType() == EntityType.PLAYER && (SGPlugin.getInstance().gm.getState() == GameState.LOBBY || SGPlugin.getInstance().gm.getState() == GameState.LOBBY_COUNTDOWN))
        {
            e.setCancelled(true);
            if (e.getCause() == EntityDamageEvent.DamageCause.VOID)
                e.getEntity().teleport(SGPlugin.spawn);
        } else
        {
            if (e.getEntity() instanceof Player)
            {
                if (SGPlugin.getInstance().gm.getPlayerState((Player) e.getEntity()) != GameManager.PlayerType.PLAYER)
                {
                    e.setCancelled(true);
                }
            }

        }
    }

    @EventHandler
    public void onHungerChange(FoodLevelChangeEvent e)
    {
        if (SGPlugin.getInstance().gm.getState() == GameState.LOBBY || SGPlugin.getInstance().gm.getState() == GameState.LOBBY_COUNTDOWN)
        {
            e.setCancelled(true);
        }
    }
}
