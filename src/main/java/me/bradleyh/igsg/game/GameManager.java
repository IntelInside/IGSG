package me.bradleyh.igsg.game;

import lombok.Getter;
import lombok.Setter;
import me.bradleyh.igsg.SGPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class GameManager
{
    @Getter
    @Setter
    public static int players = 0;
    private static int secondsLeft = 60;
    private static int secondsUntilDM = 10;
    private static BukkitTask dmCountdown = null;
    private static BukkitTask gameStartCountdown = null;
    private static double secondsUntilGame = 5;
    public BukkitTask lobbyCountdown = null;
    public ArrayList<Location> usedSpawns = new ArrayList<>();
    public ArrayList<Block> brokenBlocks = new ArrayList<>();
    public ArrayList<Block> placedBlocks = new ArrayList<>();
    private BukkitTask stageClock = null;
    private HashMap<Player, PlayerType> playerState = new HashMap<>();
    @Getter
    @Setter
    private ArrayList<Player> position = new ArrayList<>();
    private Arena currentArena = null;
    private GameState state = GameState.LOBBY;
    private GameStage stage = GameStage.PREREFILL;
    private int spawnNumber = 0;

    public static Arena getRandomArena()
    {
        Random r = new Random();
        return SGPlugin.getInstance().arenas.get(r.nextInt(SGPlugin.getInstance().arenas.size()));
    }

    public void startLobbyCountdown()
    {
        setState(GameState.LOBBY_COUNTDOWN);
    }

    public void setPlayerState(Player p, PlayerType type)
    {
        if (playerState.containsKey(p)) playerState.remove(p);
        playerState.put(p, type);
        if (state == GameState.INGAME)
        {
            if (type == PlayerType.PLAYER)
            {
                p.setGameMode(GameMode.SURVIVAL);
            } else
            {
                p.setGameMode(GameMode.SPECTATOR);
            }
        }
    }

    public PlayerType getPlayerState(Player p)
    {
        return playerState.get(p);
    }

    public void startGame()
    {
        setStage(GameStage.PREREFILL);
        for (Player p : Bukkit.getOnlinePlayers())
        {
            if (playerState.get(p) == PlayerType.PLAYER)
            {
                p.teleport(currentArena.getSpawns().get(spawnNumber));
                spawnNumber = spawnNumber + 1;
            } else
            {
                p.teleport(getCurrentArena().getCenter());
            }
        }
        spawnNumber = 0;
        setState(GameState.GAME_STARTING);
    }

    public void stopGame()
    {
        setState(GameState.GAME_ENDING);
    }

    public GameState getState()
    {
        return this.state;
    }

    public void setState(GameState state)
    {
        this.state = state;
        if (state == GameState.LOBBY)
        {
            for (Player p : Bukkit.getOnlinePlayers())
            {
                p.teleport(SGPlugin.spawn);
                p.setGameMode(GameMode.SURVIVAL);
            }
            if (Bukkit.getOnlinePlayers().size() >= 12)
            {
                setCurrentArena(getRandomArena());
                startLobbyCountdown();
            }
        }

        if (state == GameState.LOBBY_COUNTDOWN)
        {
            ArrayList<Integer> bcNumbers = new ArrayList<Integer>()
            {{
                add(60);
                add(50);
                add(40);
                add(30);
                add(20);
                add(10);
                add(5);
                add(4);
                add(3);
                add(2);
                add(1);
            }};

            lobbyCountdown = Bukkit.getScheduler().runTaskTimer(SGPlugin.getInstance(), () ->
            {
                if (secondsLeft == 0)
                {
                    secondsLeft = 60;
                    startGame();
                    lobbyCountdown.cancel();
                } else
                {
                    if (bcNumbers.contains(secondsLeft))
                    {
                        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&a&lGAME &r&8// &r&7The game will start in &a" + secondsLeft + " &7seconds."));
                    }
                    secondsLeft--;
                }
            }, 0L, 20L);
        }

        if (state == GameState.GAME_STARTING)
        {
            Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&2&m========&r &a&lSurvival Games &2&m========&r\n" +
                    "\n" +
                    "    &a&lSurvival Games &r&8- &7Game Start\n" +
                    "\n" +
                    "    &7Collect materials and fight,\n" +
                    "    &7but be careful! You're not\n" +
                    "    &7the only one on a mission...\n" +
                    "\n" +
                    "&2&m================================"));
            setStage(GameStage.PREREFILL);

            players = 0;

            for (Player p : Bukkit.getOnlinePlayers())
            {
                if (getPlayerState(p) == PlayerType.PLAYER)
                {
                    p.setGameMode(GameMode.SURVIVAL);
                    players = players + 1;
                } else
                {
                    p.setGameMode(GameMode.SPECTATOR);
                }
            }
            SGPlugin.resetChests();

            gameStartCountdown = Bukkit.getScheduler().runTaskTimer(SGPlugin.getInstance(), () ->
            {
                if (secondsUntilGame <= 0)
                {
                    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&a&lGAME &r&8// &7The game has started."));
                    setState(GameState.INGAME);

                    stageClock = Bukkit.getScheduler().runTaskLater(SGPlugin.getInstance(), () -> setStage(GameStage.REFILL), 20L * 60 * 10);
                    gameStartCountdown.cancel();
                }
                secondsUntilGame = secondsUntilGame - .1;
            }, 0, 2L);
        }

        if (state == GameState.INGAME)
        {
            for (Player p : Bukkit.getOnlinePlayers())
            {
                p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 60, 2, true, false));
            }
        }

        if (state == GameState.GAME_ENDING)
        {
            if (dmCountdown != null)
            {
                dmCountdown.cancel();
            }
            secondsUntilDM = 10;
            stageClock.cancel();
            secondsUntilGame = 5;
            Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&2&m========&r &a&lSurvival Games &2&m========&r\n" +
                    "\n" +
                    "    &a&lSurvival Games &r&8- &7Game Over\n" +
                    "\n" +
                    (position.size() > 0 && position.get(0) != null ? "    &4&l1ST PLACE &r&8// &7" + position.get(0).getName() + "\n" : "\n") +
                    (position.size() > 1 && position.get(1) != null ? "    &6&l2ND PLACE &r&8// &7" + position.get(1).getName() + "\n" : "\n") +
                    (position.size() > 2 && position.get(2) != null ? "    &e&l3RD PLACE &r&8// &7" + position.get(2).getName() + "\n" : "\n") +
                    "\n" +
                    "    &a&lMAP &r&8// &7" + currentArena.getName() + "\n" +
                    "\n" +
                    "&2&m================================"));
            Bukkit.getScheduler().runTaskLater(SGPlugin.getInstance(), () ->
            {
                for (Player p : Bukkit.getOnlinePlayers())
                {
                    p.getInventory().clear();
                    p.teleport(SGPlugin.spawn);
                    p.setGameMode(GameMode.SURVIVAL);
                    if (getPlayerState(p) != PlayerType.SITOUT)
                    {
                        setPlayerState(p, PlayerType.PLAYER);
                    }
                    p.setFoodLevel(20);
                }
                setState(GameState.LOBBY);
                restoreArena();
                setCurrentArena(getRandomArena());
            }, 70L);
        }
    }

    public GameStage getStage()
    {
        return this.stage;
    }

    public void setStage(GameStage stage)
    {
        this.stage = stage;
        if (getState() == GameState.GAME_STARTING || getState() == GameState.INGAME)
        {
            if (stage == GameStage.PREDM)
            {
                for (Player p : Bukkit.getOnlinePlayers())
                {
                    if (playerState.get(p) == PlayerType.PLAYER)
                    {
                        if (!usedSpawns.contains(getCurrentArena().getSpawns().get(spawnNumber)))
                        {
                            p.teleport(getCurrentArena().getSpawns().get(spawnNumber));
                            usedSpawns.add(getCurrentArena().getSpawns().get(spawnNumber));
                            spawnNumber = spawnNumber + 1;
                        } else
                        {
                            do
                            {
                                p.teleport(getCurrentArena().getSpawns().get(spawnNumber));
                                usedSpawns.add(getCurrentArena().getSpawns().get(spawnNumber));
                                spawnNumber = spawnNumber + 1;
                            }
                            while (!usedSpawns.contains(getCurrentArena().getSpawns().get(spawnNumber)));
                        }
                    }
                }

                spawnNumber = 0;

                dmCountdown = Bukkit.getScheduler().runTaskTimer(SGPlugin.getInstance(), () ->
                {
                    if (secondsUntilDM == 0)
                    {
                        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&a&lDM &r&8// &7Deathmatch has started."));
                        secondsUntilDM = 10;
                        setStage(GameStage.DM);
                        dmCountdown.cancel();
                    }
                    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&a&lDM &r&8// &7Deathmatch will start in &a" + secondsUntilDM + "&7 seconds."));
                    secondsUntilDM--;

                }, 0, 20L);
            }

            if (stage == GameStage.REFILL)
            {
                SGPlugin.resetChests();
                Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&a&lDM &r&8// &7Chests have been refilled."));
                stageClock = Bukkit.getScheduler().runTaskLater(SGPlugin.getInstance(), () ->
                {
                    setStage(GameStage.PREDM);
                }, 20L * 60 * 5);
            }

            if (stage == GameStage.DM)
            {
                stageClock = Bukkit.getScheduler().runTaskLater(SGPlugin.getInstance(), () ->
                {
                    setState(GameState.GAME_ENDING);
                }, 20L * 60 * 5);
                currentArena.setWorldBorder(50, currentArena.center, 2);
            }
        }
    }

    public void removePlayerState(Player p)
    {
        playerState.remove(p);
    }

    public Arena getCurrentArena()
    {
        return currentArena;
    }

    public void setCurrentArena(Arena arena)
    {
        if (getState() == GameState.LOBBY || getState() == GameState.LOBBY_COUNTDOWN)
        {
            this.currentArena = arena;
        }
    }

    public void restoreArena()
    {
        SGPlugin.mvCore.deleteWorld(getCurrentArena().getWorldName());
        SGPlugin.mvCore.cloneWorld(getCurrentArena().getWorld(), getCurrentArena().getWorldName(), "NORMAL");
        SGPlugin.mvCore.getMVWorldManager().loadWorld(getCurrentArena().getWorldName());
        SGPlugin.mvCore.getMVWorldManager().getMVWorld(getCurrentArena().getWorldName()).setAllowAnimalSpawn(false);
        SGPlugin.mvCore.getMVWorldManager().getMVWorld(getCurrentArena().getWorldName()).setAllowMonsterSpawn(false);
    }

    public void addBlockToRestore(Block b, boolean broken)
    {
        if (!broken)
            placedBlocks.add(b);
        else
            brokenBlocks.add(b);
    }

    public enum PlayerType
    {
        PLAYER,
        SPECTATOR,
        SITOUT,
        JOIN_SPECTATOR
    }
}
