package me.bradleyh.igsg.game;

import lombok.Data;
import org.apache.commons.lang3.Range;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import java.util.ArrayList;

@Data
public class Arena
{
    double x1;
    double x2;
    double y1;
    double y2;
    double z1;
    double z2;
    String worldName;
    String world;
    String name;
    ArrayList<Location> spawns;
    Location center;

    public Arena(double x1, double y1, double z1, double x2, double y2, double z2, String world, String name, ArrayList<Location> spawns, Location center)
    {
        if (Bukkit.createWorld(new WorldCreator(worldName + "_tmp")) != null)
        {
            this.world = world;
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;
            this.z1 = z1;
            this.z2 = z2;
            this.name = name;
            this.spawns = spawns;
            this.center = center;
            this.worldName = world + "_tmp";
        } else
        {
            throw new NullPointerException("World is null!");
        }
    }

    public boolean isInArena(Location l)
    {
        if (Range.between(x1, x2).contains(l.getX()))
        {
            if (Range.between(y1, y2).contains(l.getY()))
            {
                return Range.between(z1, z2).contains(l.getZ());
            } else return false;
        } else return false;
    }

    public void setWorldBorder(double radius, Location center, double damage)
    {
        World world = Bukkit.getWorld(worldName);
        world.getWorldBorder().setSize(radius);
        world.getWorldBorder().setCenter(center);
        world.getWorldBorder().setDamageAmount(damage);
    }

    public void resetWorldBorder()
    {
        Bukkit.getWorld(worldName).getWorldBorder().reset();
    }

    public String getName()
    {
        return name;
    }

    public ArrayList<Location> getSpawns()
    {
        return spawns;
    }

    public Location getCenter()
    {
        return center;
    }
}
