package me.bradleyh.igsg.game;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class RegenEngine
{
    public Inventory getChestInventory()
    {
        Inventory inv = Bukkit.createInventory(null, 27);
        Random r = new Random();
        for (int i = 0; i < 3; i++)
        {
            final HashMap<ItemStack, Rarity> items = new HashMap<ItemStack, Rarity>()
            {{
                // Common Items
                put(new ItemStack(Material.ARROW, 2), Rarity.COMMON);
                put(new ItemStack(Material.FEATHER, 2), Rarity.COMMON);
                put(new ItemStack(Material.FLINT, 2), Rarity.COMMON);
                put(new ItemStack(Material.EGG, 4), Rarity.COMMON);
                put(new ItemStack(Material.WOOD_AXE, 1), Rarity.COMMON);
                put(new ItemStack(Material.COOKIE, 2), Rarity.COMMON);
                put(new ItemStack(Material.CARROT, 2), Rarity.COMMON);
                put(new ItemStack(Material.COOKED_BEEF, 3), Rarity.COMMON);
                // Uncommon Items
                put(new ItemStack(Material.CAKE, 1), Rarity.UNCOMMON);
                put(new ItemStack(Material.WOOD_SWORD, 1), Rarity.UNCOMMON);
                put(new ItemStack(Material.LEATHER_BOOTS, 1), Rarity.UNCOMMON);
                put(new ItemStack(Material.LEATHER_HELMET, 1), Rarity.UNCOMMON);
                put(new ItemStack(Material.LEATHER_CHESTPLATE, 1), Rarity.UNCOMMON);
                put(new ItemStack(Material.LEATHER_LEGGINGS, 1), Rarity.UNCOMMON);
                put(new ItemStack(Material.MUSHROOM_SOUP, 1), Rarity.UNCOMMON);
                put(new ItemStack(Material.STICK, 1), Rarity.UNCOMMON);
                // Rare Items
                put(new ItemStack(Material.STONE_SWORD, 1), Rarity.RARE);
                put(new ItemStack(Material.CHAINMAIL_BOOTS, 1), Rarity.RARE);
                put(new ItemStack(Material.CHAINMAIL_HELMET, 1), Rarity.RARE);
                put(new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1), Rarity.RARE);
                put(new ItemStack(Material.CHAINMAIL_LEGGINGS, 1), Rarity.RARE);
                put(new ItemStack(Material.BOAT, 1), Rarity.RARE);
                // Extraordinary Items
                put(new ItemStack(Material.IRON_SWORD, 1), Rarity.EXTRAORDINARY);
                put(new ItemStack(Material.IRON_BOOTS, 1), Rarity.EXTRAORDINARY);
                put(new ItemStack(Material.IRON_HELMET, 1), Rarity.EXTRAORDINARY);
                put(new ItemStack(Material.IRON_CHESTPLATE, 1), Rarity.EXTRAORDINARY);
                put(new ItemStack(Material.IRON_LEGGINGS, 1), Rarity.EXTRAORDINARY);
                put(new ItemStack(Material.DIAMOND, 1), Rarity.EXTRAORDINARY);
                // Legendary Items
                put(new ItemStack(Material.DIAMOND_SWORD, 1), Rarity.LEGENDARY);
                put(new ItemStack(Material.DIAMOND_BOOTS, 1), Rarity.LEGENDARY);
                put(new ItemStack(Material.DIAMOND_HELMET, 1), Rarity.LEGENDARY);
                put(new ItemStack(Material.DIAMOND_CHESTPLATE, 1), Rarity.LEGENDARY);
                put(new ItemStack(Material.DIAMOND_LEGGINGS, 1), Rarity.LEGENDARY);
                put(new ItemStack(Material.ELYTRA, 1), Rarity.LEGENDARY);
            }};

            Rarity rarity = getRandomRarity();
            ArrayList<ItemStack> possibleItems = new ArrayList<>();
            for (ItemStack item : items.keySet())
            {
                if (items.get(item) == rarity)
                {
                    possibleItems.add(item);
                }
            }
            int itemVal = r.nextInt(possibleItems.size());
            inv.addItem(formatItem(possibleItems.get(itemVal), rarity));
        }
        return inv;
    }

    private Rarity getRandomRarity()
    {
        Random r = new Random();
        int rarityVal = r.nextInt(100);

        return getRarityFromInt(rarityVal);
    }

    private Rarity getRarityFromInt(int rarityVal)
    {
        Rarity rarity;
        if (rarityVal >= 98)
        {
            rarity = Rarity.LEGENDARY;
        } else if (rarityVal >= 94)
        {
            rarity = Rarity.EXTRAORDINARY;
        } else if (rarityVal >= 75)
        {
            rarity = Rarity.RARE;
        } else if (rarityVal >= 60)
        {
            rarity = Rarity.UNCOMMON;
        } else
        {
            rarity = Rarity.COMMON;
        }
        return rarity;
    }

    private ItemStack formatItem(ItemStack item, Rarity rarity)
    {
        ChatColor color = ChatColor.GRAY;
        switch (rarity)
        {
            case COMMON:
                color = ChatColor.GREEN;
                break;
            case UNCOMMON:
                color = ChatColor.DARK_AQUA;
                break;
            case RARE:
                color = ChatColor.BLUE;
                break;
            case EXTRAORDINARY:
                color = ChatColor.GOLD;
                break;
            case LEGENDARY:
                color = ChatColor.DARK_RED;
                break;
        }

        ItemStack newItem = item;
        ItemMeta newItemMeta = newItem.getItemMeta();
        newItemMeta.setDisplayName(color + "" + ChatColor.BOLD + rarity.toString().toUpperCase() + ChatColor.RESET + ChatColor.GRAY + " " + item.getType().toString().replace('_', ' '));
        newItem.setItemMeta(newItemMeta);
        return newItem;
    }

    public enum Rarity
    {
        COMMON(0),
        UNCOMMON(60),
        RARE(75),
        EXTRAORDINARY(94),
        LEGENDARY(98);

        int rarity;

        Rarity(int rarityValue)
        {
            this.rarity = rarityValue;
        }

        public int getRarity()
        {
            return this.rarity;
        }
    }
}
