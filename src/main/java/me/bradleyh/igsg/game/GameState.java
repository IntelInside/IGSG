package me.bradleyh.igsg.game;

public enum GameState
{
    LOBBY,
    LOBBY_COUNTDOWN,
    GAME_STARTING,
    INGAME,
    GAME_ENDING
}

