package me.bradleyh.igsg;

import com.onarandombox.MultiverseCore.MultiverseCore;
import lombok.Getter;
import me.bradleyh.igsg.command.GameCommand;
import me.bradleyh.igsg.game.Arena;
import me.bradleyh.igsg.game.GameManager;
import me.bradleyh.igsg.game.RegenEngine;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class SGPlugin extends JavaPlugin
{
    public static Location spawn;
    public static MultiverseCore mvCore;
    static ArrayList<Block> usedChests = new ArrayList<>();
    private static SGPlugin instance;
    public ArrayList<Arena> arenas;
    public GameManager gm;
    @Getter
    private RegenEngine regenEngine = new RegenEngine();

    public static void resetChests()
    {
        usedChests.clear();
    }

    public static SGPlugin getInstance()
    {
        return instance;
    }

    @Override
    public void onEnable()
    {
        mvCore = getMultiverseCore();

        instance = this;

        gm = new GameManager();

        arenas = getArenas();
        mvCore.getMVWorldManager().loadWorld(YamlConfiguration.loadConfiguration(new File("plugins/IGSG/locations.yml")).getString("spawn").split(":")[0]);
        mvCore.getMVWorldManager().setFirstSpawnWorld(YamlConfiguration.loadConfiguration(new File("plugins/IGSG/locations.yml")).getString("spawn").split(":")[0]);
        if (arenas.get(0) == null)
        {
            Bukkit.getLogger().warning("NO ARENAS FOUND!");
        }

        Bukkit.getPluginManager().registerEvents(new Listeners(), this);

        spawn = LocationSerializer.deserialize(YamlConfiguration.loadConfiguration(new File("plugins/IGSG/locations.yml")).getString("spawn"));

        getCommand("game").setExecutor(new GameCommand());

        gm.setCurrentArena(GameManager.getRandomArena());
    }

    public MultiverseCore getMultiverseCore()
    {
        Plugin plugin = getServer().getPluginManager().getPlugin("Multiverse-Core");

        if (plugin instanceof MultiverseCore)
        {
            return (MultiverseCore) plugin;
        }

        throw new RuntimeException("MultiVerse not found!");
    }

    void addChest(Block block)
    {
        if (!usedChests.contains(block) && block.getType() == Material.ENDER_CHEST)
        {
            usedChests.add(block);
        }
    }

    private ArrayList<Arena> getArenas()
    {
        File folder = new File("plugins/IGSG/arenas");
        File[] files = folder.listFiles();
        ArrayList<Arena> arenas = new ArrayList<>();

        for (File f : files)
        {
            if (!f.isDirectory())
            {
                YamlConfiguration config = YamlConfiguration.loadConfiguration(f);
                List<String> spawnNode = config.getStringList("spawns");
                ArrayList<Location> spawns = new ArrayList<>();
                for (String spawn : spawnNode)
                {
                    if (Bukkit.getWorld(spawn.split(":")[0] + "_tmp") == null)
                    {
                        mvCore.cloneWorld(spawn.split(":")[0], spawn.split(":")[0] + "_tmp", "NORMAL");
                    }
                    Location l = LocationSerializer.deserialize(spawn);
                    l.setWorld(Bukkit.getWorld(spawn.split(":")[0] + "_tmp"));
                    spawns.add(l);
                }
                Bukkit.getLogger().info("Loaded " + spawns.size() + " spawns for arena " + config.getString("name") + ".");

                arenas.add(new Arena(config.getDouble("x1"), config.getDouble("y1"), config.getDouble("z1"), config.getDouble("x2"), config.getDouble("y2"), config.getDouble("z2"), config.getString("world"), config.getString("name"), spawns, LocationSerializer.deserialize(config.getString("center"))));
            }
        }
        return arenas;
    }

    @Override
    public void onDisable()
    {
        for (Block b : gm.placedBlocks)
        {
            b.setType(Material.AIR);
        }

        for (Block b : gm.brokenBlocks)
        {
            b.setType(b.getType());
        }
        for (Player p : Bukkit.getOnlinePlayers())
        {
            gm.removePlayerState(p);
            p.kickPlayer("Server stopped.");
        }

        mvCore.deleteWorld(gm.getCurrentArena().getWorldName());
    }
}
