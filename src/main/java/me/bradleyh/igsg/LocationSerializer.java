package me.bradleyh.igsg;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationSerializer
{

    public static Location deserialize(String location)
    {
        String[] parts = location.split(":");
        return new Location(Bukkit.getWorld(parts[0]), Double.parseDouble(parts[1]), Double.parseDouble(parts[2]), Double.parseDouble(parts[3]));
    }

    public static String serialize(Location location)
    {
        return location.getWorld().getName() + ":" + location.getX() + ":" + location.getY() + ":" + location.getZ();
    }
}
