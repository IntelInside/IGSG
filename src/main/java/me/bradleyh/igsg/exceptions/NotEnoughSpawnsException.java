package me.bradleyh.igsg.exceptions;

public class NotEnoughSpawnsException extends Exception
{
    public NotEnoughSpawnsException()
    {
        super("Not enough spawns are present!");
    }
}
