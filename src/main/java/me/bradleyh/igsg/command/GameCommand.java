package me.bradleyh.igsg.command;

import me.bradleyh.igsg.LocationSerializer;
import me.bradleyh.igsg.SGPlugin;
import me.bradleyh.igsg.game.GameManager;
import me.bradleyh.igsg.game.GameState;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class GameCommand implements CommandExecutor
{

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        if (sender instanceof Player)
        {
            if (args[0].equalsIgnoreCase("spec"))
            {
                if (SGPlugin.getInstance().gm.getState() == GameState.LOBBY || SGPlugin.getInstance().gm.getState() == GameState.LOBBY_COUNTDOWN)
                {
                    SGPlugin.getInstance().gm.setPlayerState((Player) sender, GameManager.PlayerType.SITOUT);
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lGAME &r&8// &7You have been set to sit out."));
                } else
                {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lGAME &r&8// &7The game is currently in progress, please wait."));
                }
            }
            if (sender.hasPermission("sg.manage"))
            {
                if (args.length == 1)
                {
                    if (args[0].equalsIgnoreCase("start"))
                    {
                        if (SGPlugin.getInstance().gm.getState() == GameState.LOBBY || SGPlugin.getInstance().gm.getState() == GameState.LOBBY_COUNTDOWN)
                        {
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lGAME &r&8// &7The game has been started."));
                            if (SGPlugin.getInstance().gm.getState() == GameState.LOBBY_COUNTDOWN)
                            {
                                SGPlugin.getInstance().gm.lobbyCountdown.cancel();
                            }
                            SGPlugin.getInstance().gm.startGame();
                        } else
                        {
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lGAME &r&8// &7The game is in progress."));
                        }
                    } else if (args[0].equalsIgnoreCase("stop"))
                    {
                        if (SGPlugin.getInstance().gm.getState() == GameState.INGAME)
                        {
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lGAME &r&8// &7The game has been ended."));
                            SGPlugin.getInstance().gm.stopGame();
                        } else
                        {
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lGAME &r&8// &7The game is not in progress."));
                        }
                    } else
                    {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lGAME &r&8// &7An error occurred while processing your arguments."));
                    }
                } else if (args.length == 2)
                {
                    if (args[0].equalsIgnoreCase("addspawn"))
                    {
                        File file = new File("plugins/IGSG/arenas/" + args[1].toLowerCase() + ".yml");
                        if (file == null || !file.exists())
                        {
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lGAME &r&8// &7No file was found with that name."));
                        }
                        YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
                        ArrayList<String> spawns = (ArrayList<String>) config.getStringList("spawns");
                        spawns.add(LocationSerializer.serialize(((Player) sender).getLocation()));
                        config.set("spawns", spawns);
                        try
                        {
                            config.save(file);
                        } catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    } else
                    {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lGAME &r&8// &7An error occurred while processing your arguments."));
                    }
                } else
                {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lGAME &r&8// &7An error occurred while processing your arguments."));
                }
            } else
            {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lPERMISSIONS &r&8// &7You do not have sufficient permissions to control the game."));
            }
        }
        return false;
    }
}
